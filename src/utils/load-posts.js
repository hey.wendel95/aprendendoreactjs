export const loadPosts = async () => {
  const postReponse = fetch('https://jsonplaceholder.typicode.com/posts');

  const photosResponse = fetch('https://jsonplaceholder.typicode.com/photos');

  const [posts, photos] = await Promise.all([postReponse, photosResponse]);

  const postsJson = await posts.json();
  const photosJson = await photos.json();

  //utilizando o método map para trazer a imagem e o post juntos de maneira em que apesar dos resultados do array photos ser maior que o array de posts, o número utilizado de imagens será renderizado de acordo com o número de posts da página.

  const postsAndPhotos = postsJson.map((post, index) => {
    return { ...post, cover: photosJson[index].url };
  });

  return postsAndPhotos;
};
