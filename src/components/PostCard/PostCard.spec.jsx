import { render, screen } from '@testing-library/react';
import { PostCard } from '.';
import { postCardPropsMock } from './mock';

const props = postCardPropsMock;

describe('<PostCard />', () => {
  it('should render PostCard correctly', () => {
    render(<PostCard {...props} />);

    expect(screen.getByRole('img', { name: /TITLE 1/i })).toHaveAttribute('src', 'img/img.png');
    expect(screen.getByRole('heading', { name: 'title 1 1' })).toBeInTheDocument();
    expect(screen.getByText('body 1')).toBeInTheDocument();

    //debug é utilizado para renderizar o componente na tela, como se fosse: console.log(component)
    //debug();
  });

  it('should match snapshot', () => {
    const { container } = render(<PostCard {...props} />);

    //toMatchSnapshot tira uma foto do component e salva num diretório novo chamado snpashot para que posteriormente tenha a estrutura inicial do componente, caso haja alguma alteração ele falha no teste pois está comparando com o snapshot contido no diretório
    expect(container.firstChild).toMatchSnapshot();
  });
});
